#!/usr/bin/env python3
import subprocess
import requests
import re

"""
If a Gitlab token is provided to cookiecutter, a Gitlab repository is created for the project.
Location of the repository: 
https://gitlab.kit.edu/{{cookiecutter.gitlab_namespace}}/IBT_{{cookiecutter.ibt_account}}_{{cookiecutter.supervisor_ibt_account}}_{{cookiecutter.project_type}}
"""

if "{{cookiecutter.gitlab_token}}": 
	headers = {'PRIVATE-TOKEN': '{{cookiecutter.gitlab_token}}'}		
	dataNamespace = ('dummy', 'null') # dummy entry for namespace id
	gitlabNamespace = "{{cookiecutter.gitlab_namespace}}" 
	# Test if the Gitlab namespace exists
	if gitlabNamespace and gitlabNamespace != "uyabc":
		params = ( ('search', "{{cookiecutter.gitlab_namespace}}"), )
		response = requests.get('https://gitlab.kit.edu/api/v4/namespaces', headers=headers, params=params)
		try:
			dataNamespace = ("namespace_id", response.json()[0]["id"])
		except:
	  		print("GitLab namespace not found.")
	data = (('name', re.sub(r'[^a-zA-Z0-9_+\- \n\.]', '', '{{cookiecutter.project_name}}')), ('path', "IBT_{{cookiecutter.ibt_account}}_{{cookiecutter.supervisor_ibt_account}}_{{cookiecutter.project_type}}"), ('description', "{{cookiecutter.project_short_description}}"), dataNamespace )
	response = requests.post('https://gitlab.kit.edu/api/v4/projects', headers=headers, data=data)
	if not response.ok:
		print("Error creating the project!")
		print(response.json())
		print("Skipping initial commit")
	else:	
		project = response.json()
		gitlabNamespace = project['namespace']['path']
		subprocess.call(['git', 'init', '--initial-branch=main'])
		if "{{cookiecutter.git_protocol}}" == "SSH":
			remoteURL = 'git@gitlab.kit.edu:' + gitlabNamespace + '/IBT_{{cookiecutter.ibt_account}}_{{cookiecutter.supervisor_ibt_account}}_{{cookiecutter.project_type}}.git'
		else:
			remoteURL = 'https://gitlab.kit.edu/' + gitlabNamespace + '/IBT_{{cookiecutter.ibt_account}}_{{cookiecutter.supervisor_ibt_account}}_{{cookiecutter.project_type}}.git'
		subprocess.call(['git', 'remote', 'add', 'origin', remoteURL])
		subprocess.call(['git', 'add', '.'])
		subprocess.call(['git', 'commit', '-m', 'Initial commit'])
		subprocess.call(['git', 'push', '-u', 'origin', 'main'])
else:
	print("No GitLab token given, creating only local folder.")

