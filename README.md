# Cookiecutter template for IBT projects
In order to generate a project from this template:
- [Install cookiecutter](https://cookiecutter.readthedocs.io/en/2.0.2/installation.html)
- Familiarize yourself with the prompts (next section)
- Generate your instance of the project by either
  - running `cookiecutter https://gitlab.kit.edu/kit/ibt-public/cookiecutter-ibtprojecttemplate.git` or
  - cloning this repository and running  `cookiecutter cookiecutter-ibtprojecttemplate` from its parent directory

## What do the fields mean?
Most fields are self-explanatory, some may not:
- Personal data:
  You will be asked to add your family_name [FamilyName], your given_name [GivenName], ibt_account [ab123] and supervisor_ibt_account [ab123]
- Project types:
  - "BA": Bachelor thesis
  - "MA": Master thesis
  - "HIWI": HiWi project
  - "PHD": PhD student
  - "GUEST": project from guest user
- `gitlab_token`: If non-empty, your a new GitLab project will be created to version
control your new template instance. Generate the token in your 
[GitLab profile](https://gitlab.kit.edu/-/profile/personal_access_tokens). The 
scope needs to be `api`. The token can be used for several projects.
- `gitlab_namespace`: Do you want to create the GitLab project in your own
personal namespace (then give your username here, for example uyabc or ab1233).
- `git_protocol`: Which protocol (SSH or HTTP) should be used when intializing 
the remote for your new repository? SSH is more convenient but requires that you
[add an SSH key to your GitLab profile](https://gitlab.kit.edu/help/ssh/index.md).
See [our wiki](https://intern.ibt.kit.edu/wiki/SSH_login_without_password) for how
to create an SSH key. The protocol can be changed later using 
the `git remote set-url` command.
