# The `doc` folder

This folder should contain all text documents associated with the project:
- manuscripts (and associated pictures)
- electronic notebook (for recording experiments for example)
- documentation for source code

Don't hesitate to create subdirectories for large projects.

