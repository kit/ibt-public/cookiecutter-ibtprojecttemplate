# `tmp` folder

This folder aims to contain data that should not be put under version control.

For example:
- Generated data (rather put the script allowing to regenerate this data under version control)
- Simulation outputs (rather put the script allowing to reproduce this simulation under version control)

