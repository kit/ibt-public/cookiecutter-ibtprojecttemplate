> **TODO**: This is an example of structure for the README file.
> When using the template, replace this file by one describing your project.

# IBT project template

This is a template for new projects created at IBT.

This template can be used as a basis for organizing data in any new project at IBT.

The structure of folders and files in this template is explained in [the 2021 Neustadt follow-up document concerning Scientific computing](https://intern.ibt.kit.edu/wiki/Good_practices_for_scientific_computing)
In addition, the final manuscript of the thesis and and the material used for the presentations, in particular the intermediate and final presentations for students, must be provided in the `thesis` directory. 

Warning in case you have this project under version control using Git: by default, the directory `tmp` is listed in the file `.gitignore` hence not put under version control.


## Getting started

Modify the files `CITATION.cff` and `README.md` to make them fit your project.

## License

> By default, this project is licensed under the Open Source license Apache 2.0. Please modify it according to your needs. 
> For choosing an Open Source license for your work, see for example [the IBT wiki](https://intern.ibt.kit.edu/wiki/Good_practices_for_scientific_computing#The_LICENSE_file).
> If can't or don't want to use an Open Source license for your work, please discuss it with your supervisor.

This project is licensed under the Apache License, Version 2.0 - see the [LICENSE](LICENSE) and [NOTICE](NOTICE) files for details.

## Citing the project

You need to cite this project when using it. See [CITATION.cff](CITATION.cff) for detailed citation information.

